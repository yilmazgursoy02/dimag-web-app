import React, { Component } from 'react';
import Navbar from "./layout/Navbar";
import LoginUser from "./forms/LoginUser";
import RegisterUser from "./forms/RegisterUser";


import {BrowserRouter as Router,Route,Switch} from "react-router-dom";

export default class App extends Component {

  render() {
   
    return (
      <Router>
        <div className="container">
         <Navbar title = "Dimag Web App"/>
          <hr/>
          <Switch>
            <Route exact path = "/"  />
            <Route exact path = "/register" component = {RegisterUser} />
            <Route exact path = "/login" component = {LoginUser} />
          </Switch>

        </div>
      </Router>

      

     
     
      
    );
  }
}
