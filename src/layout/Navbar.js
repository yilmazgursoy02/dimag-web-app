import React from 'react'
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

export default function Navbar({title}) {
  
  return (
    
    <nav className="navbar-nav navbar-expand-lg navbar navbar-dark bg-primary mb-3 p-3">
      <a href="/" className="navbar-brand">{title}</a>

      <ul className="navbar-nav ml-auto">
      <li className="btn btn-outline-success">
          <Link to = "/" className = "nav-link">Home</Link>
        </li>
        <li className="btn btn-outline-success">
          <Link to = "/register" className = "nav-link">Register</Link>
       </li>
       <li className="btn btn-outline-success">
       <Link to = "/login" className = "nav-link">Login</Link>
    </li>
      
      </ul>
    
    </nav>
    
  )
}
Navbar.propTypes = {
  title : PropTypes.string.isRequired
}
Navbar.defaultProps = {
  title : "Dimag Web App"
}

