import React, { Component } from 'react'
import UserConsumer from "../context";
import axios from "axios";



export default class RegisterUser extends Component {

  state = {
      email:"",
      first_name:"",
      last_name:"",
      password:"",
      error : false
  } 
  
  validateForm = () => {
      const {email,first_name,last_name,password} = this.state;
      if (email === "" || first_name === "" || last_name === "" || password === "") {
          return false;
      }
      return true;
      
  }
  changeInput = (e) => {
      this.setState({
          
          [e.target.name] : e.target.value
      })
  }
  
  register = async (dispatch,e) => {
      e.preventDefault();
      
      const {email,first_name,last_name,password } = this.state;

      const newUser = {
          
          email,
          first_name,
          last_name,
          password
      }
      
      if (!this.validateForm()) {
          this.setState({
              error :true
          })
          return;
      }
      
      
      const response = await axios.post("http://localhost:8080/rap/1/register",newUser);


      dispatch({type : "ADD_USER",payload:response.data});

      alert("Yeni bir kullanıcı eklendi");
    
      
  } 
  render() {
    const {email,first_name,last_name,password,error} = this.state;
    return <UserConsumer>
        {
            value => {
                const {dispatch} = value;
                return (
     
                    <div className = "col-md-8 mb-4">
                    
                      <div className="card">
                          <div className="card-header">
                          <h4>User Register</h4>
                          </div>
                          
                          <div className="card-body">
                             {
                                 error ? 
                                 <div className = "alert alert-danger">
                                    Lütfen bilgilerinizi kontrol edin.
                                 </div>
                                 :null
                             }

                              <form onSubmit = {this.register.bind(this,dispatch)}>
                                  <div className="form-group">
                                      <label htmlFor="email">Email</label>
                                      <input 
                                      type="text"
                                      name = "email"
                                      id = "email"
                                      placeholder = "Email giriniz"
                                      className ="form-control"
                                      value = {email}
                                      onChange = {this.changeInput}
              
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="first_name">First name</label>
                                      <input 
                                      type="text"
                                      name = "first_name"
                                      id = "first_name"
                                      placeholder = "Adınızı giriniz"
                                      className ="form-control"
                                      value = {first_name}
                                      onChange = {this.changeInput}
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                      <label htmlFor="last_name">Last name</label>
                                      <input 
                                      type="text"
                                      name = "last_name"
                                      id = "last_name"
                                      placeholder = "Soyadınızı giriniz"
                                      className ="form-control"
                                      value = {last_name}
                                      onChange = {this.changeInput}
                                      />
                                  
                                  </div>
                                  <div className="form-group">
                                  <label htmlFor="password">Password</label>
                                      <input 
                                      type="password"
                                      name = "password"
                                      id = "password"
                                      placeholder = "Şifre giriniz"
                                      className ="form-control"
                                      value = {password}
                                      onChange = {this.changeInput}
                                      />
                                  
                                  </div>
                                  <button className = "btn btn-danger btn-block" type = "submit">Register</button>
                              
                              
                              </form>
                          </div>
                      
                      </div>
        
                    </div>
                  )
            }
        }
    
    </UserConsumer>
 
    
  }
}