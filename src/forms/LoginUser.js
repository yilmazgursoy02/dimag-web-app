import React, { Component } from 'react'
import UserConsumer from "../context";
import axios from "axios";



export default class LoginUser  extends Component {
    state = {
        email: '',
        password: ''
    }
    
    changeInput = (e) => {
        this.setState({
            
            [e.target.name] : e.target.value
        })
    }
    
    login = async (dispatch,e) => {
        e.preventDefault();
        
        const {email,password } = this.state;
  
        const login = {
            
            email,
            password
        }
        
        
        const response = await axios.post("http://localhost:8080/rap/1/authenticate",login);
  
  
        dispatch({type : "LOGIN_USER",payload:response.data});
        
        alert("Login başarılı");
      
        
    } 
    render() {
        const {email,password} = this.state;
        return <UserConsumer>
            {
                value => {
                    const {dispatch} = value;
                    return (
         
                        <div className = "col-md-8 mb-4">
                        
                          <div className="card">
                              <div className="card-header">
                              <h4>Login</h4>
                              </div>
                              
                              <div className="card-body">
    
                                  <form onSubmit = {this.login.bind(this,dispatch)}>
                                      <div className="form-group">
                                          <label htmlFor="email">Email</label>
                                          <input 
                                          type="text"
                                          name = "email"
                                          id = "email"
                                          placeholder = "Email giriniz"
                                          className ="form-control"
                                          value = {email}
                                          onChange = {this.changeInput}
                  
                                          />
                                      
                                      </div>
                                      <div className="form-group">
                                      <label htmlFor="password">Password</label>
                                          <input 
                                          type="password"
                                          name = "password"
                                          id = "password"
                                          placeholder = "Şifre giriniz"
                                          className ="form-control"
                                          value = {password}
                                          onChange = {this.changeInput}
                                          />
                                      
                                      </div>
                                      <button className = "btn btn-danger btn-block" type = "submit">Login</button>
                                  
                                  
                                  </form>
                              </div>
                          
                          </div>
            
                        </div>
                      )
                }
            }
        
        </UserConsumer>
        
        
        
        
        
      }
}

